# backend-node-challenge

Phone App
Exercise 1: Create an endpoint to retrieve the phone catalog, and pricing.
 * it returns a collection of phones, and their prices.
 * Each phone should contain a reference to its image, the name, the description,
and its price.
* Pagination support
Exercise 2: Create endpoints to check and create an order.
 * receives and order that contains the customer information name, surname, and
email, and the list of phones that the customer wants to buy.
 * Calculate the total prices of the order.
 * Log the final order to the console.
Bonus Points: The second endpoint use the first endpoint to validate the order.
Requirements:
- It should have test.
- It should be documented in the readme file.
- It should be a REST API
- Docker oriented.
- Microservice approach.
- Database access from the microservices.
- Java 10, .NET, NodeJS, Scala
Questions
- How would you improve the system?
- How would you avoid your order API to be overflow?
